var client, NoosferoClient = require('../noosfero-client');

NoosferoClient.debug = true;

var readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});

readline.question('Noosfero domain: ', function(domain) {
  readline.question('User login: ', function(login) {
    readline.question('User password: ', function(password) {
      client = new NoosferoClient(domain);
      client.getToken(login, password, function(err, client) {
        if (err) {
          console.log('Get Token Fail: '+err.message);
        }
        else {
          listFriends();
        }
      });
    });
  });
});

function listFriends() {
  client.get('people/me', function(err, data) {
    if (err) return console.log('Fail to get personal info: ' + err.message);
    client.get('people/'+data.person.id+'/friends', function(err, data) {
      if (err) return console.log('Cant list friends: ' + err.message);
      for ( person of data.people ) {
        console.log('ID: '+person.id+'\t Name: '+person.name );
      }
      process.exit(0);
    });
  });
}
