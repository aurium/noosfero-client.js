(function(context){

function NoosferoClient(domain) {
  this.domain = domain;
}

// Notice to console.log
NoosferoClient.debug = false;

// Authenticate with a Noosfero user, get the token for future requests and some
// other user information for caching.
// The callback function receives the error as the first parameter, as well
// known Node.js async style, and a self reference as the second parameter:
// `function(err, client) {  }`
NoosferoClient.prototype.getToken = function (login, password, callback) {
  var client = this;
  this.post('login',
    { login: login, password: password },
    function(err, data) {
      if (err) return callback(err, client);
      if (!data.ok) {
        if (data.error) return callback(new Error(data.error.message), client);
        //else return callback(new Error('The data do not looks ok.'), client);
        console.log('DEBUG: The data do not looks ok.');
      }
      for (var k in data) console.log('=>',k,':',data[k]);
      client.private_token = data.private_token;
      client.profile = { login: data.login };
      if (data.person) { /* TODO: get this data in `people/me` */
        client.profile.id = data.person.id;
        client.profile.identifier = data.person.identifier;
        client.profile.name = data.person.name;
        client.profile.image = data.person.image;
      }
      callback(null, client);
    }
  );
};

// GET request to the API.
// The endpoint (fist parameter) is any subpath after "//domain/api/v1/".
// It supports two call signatures:
// * With parameters: client.get('some/path', {param: 'abc'}, getResponse);
// * Without parameters: client.get('some/path', getResponse);
NoosferoClient.prototype.get = function (endpoint, p2, p3) {
  this.request('GET', endpoint, p2, p3);
};

// POST request to the API.
// The endpoint (fist parameter) is any subpath after "//domain/api/v1/".
// It supports two call signatures:
// * With parameters: client.post('some/path', {param: 'abc'}, readResponse);
// * Without parameters: client.post('some/path', readResponse);
NoosferoClient.prototype.post = function (endpoint, p2, p3) {
  this.request('POST', endpoint, p2, p3);
};

// Generic HTTP request to the API.
// The endpoint (second parameter) is any subpath after "//domain/api/v1/".
// It supports two call signatures:
// * With parameters: client.request('X', 'some/path', {param: 'abc'}, readResponse);
// * Without parameters: client.request('X', 'some/path', readResponse);
NoosferoClient.prototype.request = function (method, endpoint, p2, p3) {
  if (!this.private_token && endpoint!='login') {
    throw new Error('You must run `getToken` before!');
  }
  var params = {},
      callback = function(){},
      uri = this.domain + '/api/v1/' + endpoint;
  if (p2) {
    if (p2.constructor == Function) callback = p2;
    else {
      params = p2;
      if (p3) callback = p3;
    }
  }
  params.no_cache = Math.random();
  if (this.private_token) params.private_token = this.private_token;
  asyncReq(method, uri, params, callback);
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                      H e l p e r   f u n c t i o n s                      *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

function debug() {
  var args = ['=>', (new Date()).toISOString()];
  args.push.apply(args, arguments);
  if (NoosferoClient.debug) console.log.apply(console, args);
}

function asyncReq(method, uri, params, callback) {
  debug('asyncReq', method, uri, params);
  if (typeof(XMLHttpRequest)=='function') {
    // Consider browser environment
    asyncReqWeb(method, uri, params, callback);
  }
  else if (typeof(require)=='function' && require('http')) {
    // consider Node compatible environment
    asyncReqNode(method, uri, params, callback);
  }
  else {
    throw new Error('There is no XMLHttpRequest or request(http)! Where am i?');
  }
}

// Get a XMLHttpRequest instance that suports CORS;
function newXHR() {
  var xhr = new XMLHttpRequest();
  if ('withCredentials' in xhr) return xhr;
  else if (typeof XDomainRequest != "undefined") return new XDomainRequest();
  else throw new Error('This js env has no XMLHttpRequest with CORS support.');
}

function asyncReqWeb(method, uri, params, callback) {
  var req = newXHR();
  req.onload = function() {
    console.log(req.response);
    callback(null, JSON.parse(req.response));
  };
  req.onerror = function() {
    callback( new Error('Request fail: ' + req.statusText) );
  };
  req.onabort = function() {
    callback( new Error('Request aborted.') );
  };
  var serializedParams = [];
  for (var k in params) {
    serializedParams.push(encodeURIComponent(k) +'='+ encodeURIComponent(params[k]));
  }
  if ( method.toUpperCase() == 'GET' ) {
    req.open('GET', uri + '?' + serializedParams.join('&'), true);
    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    req.send(null);
  }
  else if ( method.toUpperCase() == 'POST' ) {
    req.open('POST', uri, true);
    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    req.setRequestHeader('X-Requested-With', 'XMLHTTPRequest');
    req.send(serializedParams.join('&'));
  }
  else {
    throw new Error('Unknown/Unimplemented method "'+method.toUpperCase()+'".');
  }
}

function asyncReqNode(method, uri, params, callback) {
  method = method.toUpperCase();
  if ( /^https/.test(uri) ) var http = require('https');
  else var http = require('http');
  var serializedParams = require('querystring').stringify(params);
  if ( method == 'GET' ) uri += '?' + serializedParams;
  var options = require('url').parse(uri);
  options.method = method;
  options.keepAlive = true;
  var req = http.request(options, function(res) {
    if (res.statusCode<200 || res.statusCode>299) {
      console.log('>> ERR', res.headers);
      return callback( new Error('HTTP Error '+res.statusCode) );
    }
    res.setEncoding('utf8');
    var data = '';
    res.on('data', function (chunk) {
      data += chunk.toString();
    });
    res.on('end', function() {
      callback(null, JSON.parse(data));
    });
  });
  req.on('error', function(err) {
    callback(err);
  });
  if ( method == 'POST' ) {
    req.write(serializedParams);
  }
  else if ( method != 'GET' ) {
    throw new Error('Unknown/Unimplemented method "'+method+'".');
  }
  req.end();
}

// Export it //////////////////////////////////////////////////////////////////
if ( typeof(module)=='object' && module.exports ) module.exports = NoosferoClient;
else context.NoosferoClient = NoosferoClient;

})(this);
